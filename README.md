This site is only for issues.

Code is on <a href="http://github.com/gnachman/iTerm2">Github</a>.

The main project page is at <a href="https://iterm2.com">iterm2.com</a>.

Want to report a bug? You can do so <a href="https://iterm2.com/bugs">here</a>.